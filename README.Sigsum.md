# sigsum.org fork of the Safefile package

This fork is intended to add support for a CommitIfNotExists function,
see https://github.com/dchest/safefile/issues/9. It is maintained by
the Sigsum project.

Hopefully, changes can eventually be upstreamed to
github.com/dchest/safefile.
